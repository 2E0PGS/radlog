# RadLog

Scrobbles TuneIn radio stations to Last.FM.

Designed to be called via `cron`.

Define new TuneIn stations via config. The TuneIn station must supply artist and song names.

Requires Python3.

## How to install

* Clone this repo: `git clone https://2E0PGS@bitbucket.org/2E0PGS/radlog.git`
* Edit `config.example.json` and add relevent data. Then save it as `config.json`
* Bring down the python3 deps: `sudo pip3 install pylast`
* Use python3 to call the script via shell or cron: `python3 radlog.py`
