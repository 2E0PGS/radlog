# Scrobbles TuneIn radio stations to Last.FM

# Copyright (C) <2019-2020>  <Peter Stevenson> (2E0PGS)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
import json
import sys
import pylast
import urllib.request

# Pull in JSON config.
seperator = " - "
configPath = "config.json"
jsonFile = open(configPath, "r")
config = json.load(jsonFile)
jsonFile.close()

# Handles LastFM API.
def scrobble_track(key, secret, username, password, artist, title):
	password_hash = pylast.md5(password)
	network = pylast.LastFMNetwork(api_key=key, api_secret=secret, username=username, password_hash=password_hash)
	scrobbled = network.scrobble(artist, title, int(time.time()))

# Handles TuneIn API.
def retrive_now_playing(endpoint):
	jsonurl = urllib.request.urlopen(endpoint)
	tune_in_data = json.loads(jsonurl.read())
	#print(tune_in_data)
	subtitle = tune_in_data['Header']['Subtitle']
	if seperator in subtitle:
		artist, title = subtitle.split(seperator)
		now_playing = dict()
		now_playing['artist'] = artist
		now_playing['title'] = title
		print("Now playing: " + subtitle)
		return now_playing
	else:
		print("Not playing a real track: " + subtitle)
		return None

# Updates configs last played.
def update_config(artist, title, index):
	config['stations'][index]['lastPlayed'] = artist + seperator + title
	jsonFile = open(configPath, "w+")
	jsonFile.write(json.dumps(config))
	jsonFile.close()
	print("Updated Last.FM")

i = 0
for item in config['stations']:
	print("Found station: " + item['name'])
	now_playing = retrive_now_playing(item['endpoint'])
	if now_playing is not None:
		if (now_playing['artist'] + seperator + now_playing['title']) != item['lastPlayed']:
			scrobble_track(item['apiKey'], item['apiSecret'], item['username'], item['password'], now_playing['artist'], now_playing['title'])
			update_config(now_playing['artist'], now_playing['title'], i)
		else:
			print("No update needed.")
	i = i + 1
